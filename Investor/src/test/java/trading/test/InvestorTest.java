package trading.test;

import com.citi.trading.*;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.*;

import static org.mockito.Mockito.*;

import java.util.function.Consumer;

public class InvestorTest {
	OrderPlacer market;
	Investor investor;
	
	public class MockMarket implements OrderPlacer {
		public void placeOrder(Trade trade, Consumer<Trade> callback) {
			callback.accept(trade);
		}
	}



	@Before
	public void setup() {
		market = new MockMarket();

	}

	@Test
	public void testBuy() {
	// Create investor object with an initial balance of 10000 dollars
	investor = new Investor(1000, market);
	investor.buy("MRK", 100, 60);

	assertThat(investor.getPortfolio(), hasEntry("MRK",100));
	assertThat(investor.getPortfolio().get("MRK"), is(100));
	}

	@After
	public void destroyObjects() {
		investor = null;

		market = null;
	}

}
