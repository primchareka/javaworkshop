/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package com.mockito;

/**
Generates a Fibonacci sequence of a given length.

@author Will Provost
*/
public class Fibonacci
{
    private BoundedSequence handler;
    private int state1 = 0;
    private int state2 = 1;

    /**
    Registers a handler, which will be called with sequence events.
    */
    public void setHandler (BoundedSequence handler)
    {
        this.handler = handler;
    }

    /**
    Generates a Fibonacci sequence of a given length,
    calling the registered handler at the beginning and end and for
    each number in the sequence.
    */
    public void run (int length)
    {
        handler.start ();

        int temp = -1;
        for (int i = 0; i < length; ++i)
        {
            handler.next (state2);

            temp = state1 + state2;
            state1 = state2;
            state2 = temp;
        }

        handler.end ();
    }
}
