package com.citi.trading.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring
 * bean, this component will make HTTP requests on a 15-second timer using
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service.
 * 
 * @author Will Provost
 */
public class Pricing {

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	private final List<Observer> subscribers = new ArrayList<>();
	//private static final Logger LOG = Logger.getLogger(Pricing.class.getName());

	public static PricePoint parsePricePoint(String CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String[] fields = CSV.split(",");
		if (fields.length < 6) {
			throw new IllegalArgumentException("There must be at least 6 comma-separated fields: '" + CSV + "'");
		}

		try {
			Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
			double open = Double.parseDouble(fields[1]);
			double high = Double.parseDouble(fields[2]);
			double low = Double.parseDouble(fields[3]);
			double close = Double.parseDouble(fields[4]);
			int volume = Integer.parseInt(fields[5]);

			return new PricePoint(timestamp, open, high, low, close, volume);
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't parse timestamp.", ex);
		}
	}

	/**
	 * Requests data from the HTTP service and returns it to the caller.
	 */
	public List<PricePoint> getPriceData(String symbol, int periods) {
		List<PricePoint> pricePoints = new ArrayList<>();
		try {
			String requestURL = "http://will1.conygre.com:8081/prices/" + symbol + "?periods=" + periods;
			BufferedReader in = new BufferedReader(new InputStreamReader(new URL(requestURL).openStream()));

			in.readLine(); // header ... right? No way that could break ...
			String line = in.readLine();
			while (line != null) { 
				System.out.println(line);
			
				PricePoint price = Pricing.parsePricePoint(line);
				price.setStock(symbol);
				pricePoints.add(price);
				line = in.readLine();
			}
			return pricePoints;
		} catch (IOException ex) {
			throw new RuntimeException("Couldn't retrieve price for " + symbol + ".", ex);
		}
	}

	
	public void addSubscriber(Observer o) {
		subscribers.add(o);
	}
	
	public boolean removeSubscriber(Observer o) {
		return subscribers.remove(o);
	}
	
	public List<PricePoint> notifySubscribers(){
		for(Observer o: subscribers) {
			o.update();	
		}
		return null;
		
	}
	
	/**
	 * Quick test of the component.
	 */
	public static void main(String[] args) {
		Pricing pricing = new Pricing();
		List<PricePoint> MRKPrice = pricing.getPriceData("MRK", 12);
		System.out.println(MRKPrice);
	}
}
