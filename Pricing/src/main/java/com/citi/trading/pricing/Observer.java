package com.citi.trading.pricing;

import java.util.List;

public interface Observer {
	public List<PricePoint>update();
}
