package jUnitTestingPractice.test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import jUnitTestingPractice.MyClass;

public class MyClassTest {
	
	private MyClass setupTarget;
	
	@Before
	public void setUp() {
		setupTarget = new MyClass(4);
	}
	@Test
	public void testGetSquare() {

		assertEquals(16, setupTarget.getSquare());
	}
	
	@Test
	public void testGetSquareRoot() {

		
		//assertThat(setupTarget.getSquareRoot(),closeTo(2.0,0.00001));
	}
}
