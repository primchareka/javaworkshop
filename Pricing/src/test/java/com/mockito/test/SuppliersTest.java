/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package com.mockito.test;

import static org.junit.Assert.assertEquals;

import java.util.function.IntSupplier;

import org.junit.Test;
import org.mockito.Mockito;

/**
Comparison of methods of creating mock objects.

@author Will Provost
*/
public class SuppliersTest
{
    /**
    Test for an IntSupplier, expecting it to return a specific number.
    */
    public void testSupplier (IntSupplier supplier)
        throws Exception
    {
        assertEquals (100, supplier.getAsInt ());
    }

    /**
    Test an anonymous-class implementation of the target interface.
    */
    @Test
    public void testAnonymousClass ()
        throws Exception
    {
        testSupplier (new IntSupplier ()
            {
                public int getAsInt ()
                {
                    return 100;
                }
            } );
    }

    /**
    Test a lambda-expression implementation of the target interface.
    */
    @Test
    public void testLambda ()
        throws Exception
    {
        testSupplier (() -> 100);
    }

    /**
    Test a Mockito implementation of the target interface.
    */
    @Test
    public void testMockito ()
        throws Exception
    {
        IntSupplier supplier = Mockito.mock (IntSupplier.class);
        Mockito.when (supplier.getAsInt ()).thenReturn (100);
        testSupplier (supplier);
    }
}
