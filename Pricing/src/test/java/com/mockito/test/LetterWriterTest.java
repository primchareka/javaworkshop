/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package com.mockito.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.mockito.LetterWriter;

/**
Test for the {@link LetterWriter}.

@author Will Provost
*/
public class LetterWriterTest
{
    public static final String EXPECTED_A_FEW = "abc xyz";
    public static final String EXPECTED_A_LOT =
        "abc ABCDEFGHIJKLMNOPQRSTUVWXYZ KLMNOPQRST xyz";

    /**
    Test by creating a file, loading it again, and verifying the contents.
    */
    @Test
    public void testAFewWithRealFile ()
        throws Exception
    {
        final String filename = "Temp.txt";

        FileOutputStream out = new FileOutputStream (filename);
        LetterWriter.writeAFewLetters (out);
        // Was the file closed? We'll know, if we can't open it ourselves ...

        try
        (
            BufferedReader in =
                new BufferedReader (new FileReader (filename));
        )
        {
            assertEquals (EXPECTED_A_FEW, in.readLine ());
        }

        new File (filename).delete ();
    }

    /**
    Test by writing to an in-memory stream, and checking its contents.
    */
    @Test
    public void testAFewInMemory ()
        throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream ();
        LetterWriter.writeAFewLetters (out);
        assertEquals (EXPECTED_A_FEW, new String (out.toByteArray ()));

        // Was the stream closed? We can't really tell.
    }

    /**
    Test using a mock output stream and verifying calls to write().
    This works, because the SUT only uses one overload of write();
    by the same token, we rely on that implementation detail,
    which is dangerous ...
    */
    @Test
    public void testAFewWithVerify ()
        throws Exception
    {
        ArgumentCaptor<Integer> captor =
            ArgumentCaptor.forClass (Integer.class);
        OutputStream stream = Mockito.mock (OutputStream.class);
        LetterWriter.writeAFewLetters (stream);
        Mockito.verify (stream, Mockito.times (EXPECTED_A_FEW.length ()))
            .write (captor.capture ());
        Mockito.verify (stream).close ();

        int index = 0;
        for (int b : captor.getAllValues ())
            assertEquals (EXPECTED_A_FEW.charAt (index++), b);
    }

    /**
    Test using a mock output stream and verifying calls to write().
    This fails, because the SUT uses other overloads of write().
    We could verify a sequence of write(byte), write(byte[]), etc.,
    but then we are testing implementation when we should be testing outcome.
    */
    @Ignore
    @Test
    public void testALotWithVerify ()
        throws Exception
    {
        ArgumentCaptor<Integer> captor =
            ArgumentCaptor.forClass (Integer.class);
        OutputStream stream = Mockito.mock (OutputStream.class);
        LetterWriter.writeALotOfLetters (stream);
        Mockito.verify (stream, Mockito.times (EXPECTED_A_LOT.length ()))
            .write (captor.capture ());
        Mockito.verify (stream).close ();

        int index = 0;
        for (int b : captor.getAllValues ())
            assertEquals (EXPECTED_A_LOT.charAt (index++), b);
    }

    /**
    Forcing our way through accumulating content via Mockito "answers".
    We get a test of outcome rather than implementation, in that we don't care
    how the SUT uses the different write() overloads. But this Olympic-level
    gymnastics with Mockito, and not anywhere near worth the trouble.
    */
    @Test
    public void testALotWithDo ()
        throws Exception
    {
        class Accumulator
            implements Answer<Void>
        {
            private StringBuilder builder = new StringBuilder ();

            public Void answer (InvocationOnMock invocation)
                throws Throwable
            {
                Object[] args = invocation.getArguments ();
                if (args[0] instanceof Integer)
                    builder.append ((char) ((Integer) args[0]).intValue ());
                else if (args[0] instanceof byte[])
                {
                    if (args.length == 1)
                        builder.append (new String ((byte[]) args[0]));
                    else
                        for (int i = 0; i < (Integer) args[2]; ++i)
                            builder.append ((char)
                                ((byte[]) args[0])[(Integer) args[1] + i]);
                }

                return null;
            }

            public String getValue ()
            {
                return builder.toString ();
            }
        }
        Accumulator accumulator = new Accumulator ();

        OutputStream stream = Mockito.mock (OutputStream.class);
        Mockito.doAnswer (accumulator).when (stream)
            .write (Mockito.anyInt ());
        Mockito.doAnswer (accumulator).when (stream)
            .write ((byte[]) Mockito.anyObject ());
        Mockito.doAnswer (accumulator).when (stream)
            .write ((byte[]) Mockito.anyObject (),
                Mockito.anyInt (), Mockito.anyInt ());

        LetterWriter.writeALotOfLetters (stream);

        Mockito.verify (stream).close ();

        assertEquals (EXPECTED_A_LOT, accumulator.getValue ());
    }

    /**
    Test using an anonymous subclass of ByteArrayOutputStream.
    This gets us easy verification of outcome, along with easy verification
    of closure.
    */
    @Test
    public void testALotWithSubclass ()
        throws Exception
    {
        class VerifyingStream
            extends ByteArrayOutputStream
        {
            public boolean closed;

            @Override
            public void close ()
                throws IOException
            {
                closed = true;
                super.close ();
            }
        }

        VerifyingStream stream = new VerifyingStream ();
        LetterWriter.writeALotOfLetters (stream);
        assertEquals (EXPECTED_A_LOT, new String (stream.toByteArray ()));
        assertTrue (stream.closed);
    }

    /**
    Test using a Mockito spy over a real byte-array stream.
    This gets us easy verification of outcome, along with easy verification
    of closure.
    */
    @Test
    public void testALotWithSpy ()
        throws Exception
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream ();
        ByteArrayOutputStream spy = Mockito.spy (stream);

        LetterWriter.writeALotOfLetters (spy);
        assertEquals (EXPECTED_A_LOT, new String (spy.toByteArray ()));
        Mockito.verify (spy).close ();
    }
}
